﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SM.Core.Models
{
    public class Teacher
    {
        [Key]
        [ForeignKey("User")]
        public string Id { get; set; }

        public User User { get; set; }
    }
}
