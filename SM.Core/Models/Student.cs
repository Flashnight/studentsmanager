﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SM.Core.Models
{
    public class Student
    {
        [Key]
        [ForeignKey("User")]
        public string Id { get; set; }
        [Required]
        public string GroupId { get; set; }

        public string ParentStudentId { get; set; }

        public Group Group { get; set; }

        public User User { get; set; }

        public Student ParentStudent { get; set; }
    }
}
