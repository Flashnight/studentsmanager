﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SM.Core.Models
{
    public class Group
    {
        [Key]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<Student> Students;
        public Group()
        {
            Students = new List<Student>();
        }
    }
}
