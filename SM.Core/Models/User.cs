﻿using System.ComponentModel.DataAnnotations;

namespace SM.Core.Models
{
    public class User
    {
        [Key]
        public string Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Patronymic { get; set; }

        public Student Student { get; set; }
        public Teacher Teacher { get; set; }
    }
}
