﻿using SM.Core.Models;
using System.Data.Entity;

namespace SM.Core
{
    public class CoreContext : DbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Student> Students { get; set; }
        DbSet<Teacher> Teachers { get; set; }
        DbSet<Group> Groups { get; set; }

        public CoreContext()
            : this("SMEntities")
        {
        }

        public CoreContext(string connectionString)
            : base(connectionString)
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder mb)
        {

        }
    }
}
