﻿using System;

namespace SM.Core
{
    public static class CoreGenerator
    {
        public static string NewId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
