﻿using System.Linq;

namespace SM.Infrastructure
{
    public interface IRepository<TItem>
        where TItem : class
    {
        IQueryable<TItem> Items { get; }

        TItem AddItem(TItem item);

        TItem UpdateItem(TItem item);

        TItem Remove(TItem item);

        void Save();
    }
}
