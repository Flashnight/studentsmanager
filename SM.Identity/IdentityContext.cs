﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace SM.Identity
{
    public class IdentityContext : IdentityDbContext<User>
    {
        public IdentityContext()
            : base("SMEntities")
        {
            Init();
        }

        public IdentityContext(string connectionString)
                : base(connectionString)
        {
            Init();
        }

        private void Init()
        {
            Database.SetInitializer(new IdentityInitializer());
            //Database.Initialize(false);

            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<SlotOrder>()
            //    .HasRequired(o => o.Slot)
            //    .WithOptional(o => o.SlotOrder);
        }
    }
}
