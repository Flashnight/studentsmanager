﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace SM.Identity
{
    public class User : IdentityUser
    {
        public override string Id { get; set; }

        public DateTime Timestamp { get; set; }

        public bool Removed { get; set; }

        public User()
        {
            Id = IdentityGenerator.NewId();
            Removed = false;
            Timestamp = DateTime.UtcNow;
            SecurityStamp = Guid.NewGuid().ToString();
        }
    }
}
