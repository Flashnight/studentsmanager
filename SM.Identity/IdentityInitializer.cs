﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace SM.Identity
{
    public class IdentityInitializer : CreateDatabaseIfNotExists<IdentityContext>
    {
        protected override void Seed(IdentityContext context)
        {
            string adminRoleId = "7A422EB30BE34879BA3C8EE91F6AE665";

            string teacherRoleId = "AE48F231353F4992A8EE6CDECDEE3E5B";

            string studentRoleId = "D4F7F4FA0DE14C7ABC09F752382DF5EA";

            context.Roles.Add(new IdentityRole { Id = adminRoleId, Name = UserRoles.Admin });
            context.Roles.Add(new IdentityRole { Id = teacherRoleId, Name = UserRoles.Teacher });
            context.Roles.Add(new IdentityRole { Id = studentRoleId, Name = UserRoles.Student });

            string adminId = "52A41260A0E24390AE4C1596297715FB";

            User admin = new User
            {
                Id = adminId,
                UserName = "admin"
            };

            admin.PasswordHash = new PasswordHasher().HashPassword("admin");

            context.Users.Add(admin);
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
