﻿using System;

namespace SM.Identity
{
    static class IdentityGenerator
    {
        public static string NewId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
