﻿using System.Collections.Generic;

namespace SM.Identity
{
    public static class UserRoles
    {
        public const string Admin = "admin";

        public const string Teacher = "teacher";

        public const string Student = "student";

        public static List<string> RoleList { get; } = new List<string>
        {
            Admin,
            Teacher,
            Student
        };
    }
}
